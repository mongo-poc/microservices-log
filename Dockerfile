FROM openjdk:8-jdk-alpine
LABEL version=1.0.0 \
      group=br.com.thiagogbferreira.mongodb.reactive \
      artifact=mongodb-reactive
VOLUME /tmp
COPY target/mongodb-reactive-1.0.0.jar app.jar
ENV JAVA_OPTS=""
ENV LOGSTASH_ENDPOINT="logstash:5000"

ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar
#ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar" ]

