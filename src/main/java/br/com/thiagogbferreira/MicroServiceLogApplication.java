package br.com.thiagogbferreira;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import static net.logstash.logback.marker.Markers.*;

public class MicroServiceLogApplication {

  public static Object proceed(ProceedingJoinPoint pjp, MicroServicesContext microServicesContext) throws Throwable {
    long start = System.currentTimeMillis();
    String method = pjp.getSignature().getName();
    Logger log = LoggerFactory.getLogger(pjp.getSignature().getDeclaringType());
    MDC.put("messageId", microServicesContext.getMessageId());
    MDC.put("key", microServicesContext.getKey());
    log.info( "START {}", method );
    Object ret = pjp.proceed();
    long totalTime = System.currentTimeMillis() - start;
    if (log.isInfoEnabled()) {
      log.info(append("totalTime", totalTime), "END {}", method);
    }
    MDC.clear();
    return ret;
  }
}
