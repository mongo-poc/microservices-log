package br.com.thiagogbferreira;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@EqualsAndHashCode
@Getter
@Builder
public class MicroServicesContext {
  private final String messageId;
  private final String key;
//  private final Map<String, Object> headers;

  public MicroServicesContext(String messageId, String key) {
    this.messageId = (messageId==null || messageId.isEmpty()? UUID.randomUUID().toString():messageId);
    this.key = key;
  }

  public void fill(Map<String, String> map){
    map.put("MessageId", messageId);
    map.put("key", key);
  }

  public void fillList(Map<String, List<String>> map){
    map.put("MessageId", Collections.singletonList(messageId));
    map.put("key", Collections.singletonList(key));
  }
}
